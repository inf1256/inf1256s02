package inf1256s02;
import java.util.*;//importation des classes
public class ConversionCelciusAFahrenheit {

	public static void main(String[] args) {
		// ATTENTION: ecrire les nombres réels avec .
		Double tCel;//déclaration
		Scanner sc; //déclaration --pour lecture entrée
		 sc=new Scanner(System.in); //affectation d'une instance
		System.out.println("Entrez la temperature en degre Celcius svp");
		  tCel=sc.nextDouble();//affectation nombre reel saisi au clavier
		Double tFah = (tCel * (9/5.0))+32;
		System.out.println("La temperature en degré Fahrenheit est: "+tFah);
		sc.close(); //fermeture scanner

	}

}
